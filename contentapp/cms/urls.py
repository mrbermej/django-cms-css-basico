from .views import process, index, style
from django.urls import path

urlpatterns = [
    path("", index),
    path("css", style), # esto lo he creado nuevo para probar css
    path("<str:parametro>", process)
]