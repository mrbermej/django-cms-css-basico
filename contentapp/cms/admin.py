from django.contrib import admin
from .models import tabla, comentario

# Register your models here.
admin.site.register(tabla)
admin.site.register(comentario)
