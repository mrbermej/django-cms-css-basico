from django.shortcuts import render
from django.http import HttpResponse
from .models import tabla, comentario
from django.views.decorators.csrf import csrf_exempt #esto se usa cuando introducimos como parametro otro que ya habiamos introducido antes
from django.template import loader
import random

INITIAL_PAGE = """
<!DOCTYPE html>
<html lang="en">
<head>
    <title>cms.com</title>
</head>
    <body>
        <h1>Bienvenido a cms.com</h1>
        <p>Aquí tienes un diccionario donde guardar las claves con sus valores:</p>
        <div>
            {form}
        </div>
        <div>
            <p>Esta es la lista de claves que has metido: [{lista}]</p>
        </div>
    </body>
</html>
"""

FORM = """
<form action="" method='POST'>
    <label>Introduce lo que quieras:</label>
    <input type='text' name='clave' required>
    <input type='submit' value='Guardar'>
</form>
"""
# Create your views here.

def probando(request):
    lista = []
    method = request.method
    if method == 'GET':
        return HttpResponse(INITIAL_PAGE.format(form=FORM, lista=""))
    elif method == 'POST':
        clave = request.POST.get('clave', None)
        if clave:
            lista.append(clave)
        return HttpResponse(INITIAL_PAGE.format(form=FORM, lista=", ".join(lista)))
    else:
        return HttpResponse("<h1>No se que quieres que haga</h1>")

@csrf_exempt
def process(request, parametro):
    if request.method == 'POST':
        valor = request.POST['clave']
        #esto va a devolverme el valor de la clave que se ha introducido en el formulario
        c = tabla(clave=parametro, valor=valor)
        c.save()
        return HttpResponse(f'Guardado: clave:{parametro} // valor: {valor} ')
    #esto se usa cuando ya hemos introcido un parametro en la tabla de datos, si ponemos localhost:8000/cms/parametrointroducido saldra ese mensaje y si no está en la tabla saltara el DoesNotExist
    try:
        key = tabla.objects.get(clave=parametro)
        response = f"El valor para el parametro /{parametro}/ es /{key.valor}/"
    except tabla.objects.DoesNotExist:
        response = ("La clave que has introducido no está en la tabla,¿la quieres introducir?", FORM)
    return HttpResponse(response)


def index(request):
    #1 Tener la lista de contenidos
    contenido = tabla.objects.all()
    print("contenido de la tabla: ",contenido)
    #2 Cargar la plantilla
    template = loader.get_template('cms/index.html')
    #3 Ligar las variables de la plantilla a las variables de python
    context = {"content_list": contenido}
    print("context: ", context)
    #4 Renderizar--> es ejecutar
    return HttpResponse(template.render(context, request))

#acabo de usar mi primera plantilla css, he usado azul claro para el fondo y azul para las letras, lo que hago es enlazar la plantilla index.html con el css
#cuando en el navegador pongo localhost:8000/cms me aparecerá ya con el formato css sin tener que poner /cms/css

def style(request):
    color = random.choice(["blue", "gold", "red", "aqua", "green", "black", "yellow"])
    background = random.choice(["blue", "gold", "red", "aqua", "green", "black", "yellow"])
    contenido = """body{{
                    margin: 10px 20% 50px 70px;
                    font-family: sans-serif;
                    color: {};
                    background: {};
                    }}"""
    return HttpResponse(contenido.format(color, background), content_type='text/css')
