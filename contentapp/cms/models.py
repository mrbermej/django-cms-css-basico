from django.db import models

# Create your models here.
class tabla(models.Model):
    clave = models.CharField(max_length=20)
    valor = models.TextField()

    #el def __str__(self): es la forma en la que va a aparecer el contenido en el administrador de django
    def __str__(self):
        return str(self.id) + ": " + self.clave + '---> ' + self.valor

    #esta función es un booleano que comprueba si las claves o valores que hemmos introducido en la tabla tiene la letra "a"
    def tiene_a(self):
        return ("a" in self.valor)


#creo otra tabla para introducir comentarios relacionados a cada contenido
class comentario(models.Model):
    contenido = models.ForeignKey(tabla, on_delete=models.CASCADE) #con el foreign key se relaciona el comentario con la clave de la tabla que estemos comentando
    title = models.CharField(max_length=20)
    comentario = models.TextField()
    fecha = models.DateTimeField()
    def __str__(self):
        return str(self.id) + ": " + "[" + str(self.fecha) + "]" + " --> " + self.title

